import React, { useState } from "react";
import extractPdf from './extractPdf.js';
import renderPdf from './renderPdf.js';
import RenderDocument from "./renderDocument.js";


var pdfData = null;

export default function LoadFile() {

    const reader = new FileReader();
    var [dataReady, setDataReady] = useState(false);

    // Load pdf file
    const onLoadPdf = (e) => {
        reader.readAsArrayBuffer(e.target.files[0]);
        reader.onload = () => {
            //pdfData = extractPdf(new Uint8Array(reader.result));
            renderPdf(new Uint8Array(reader.result));
            setDataReady(true);
        };
    };

    return !dataReady ? (
        <div id="dropbox" className="dropbox">
            <input onChange={onLoadPdf} type="file" />
        </div> 
    ) : <RenderDocument pdfData={pdfData}/>
};