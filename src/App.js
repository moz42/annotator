import React from 'react';
import './App.css';
import LoadFile from './loadFile.js';

export default function App() {
	return (
		<div className="App">
			<LoadFile  />
		</div>
	)
};