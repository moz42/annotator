import PDFJSWorker from "worker-loader!pdfjs-dist/build/pdf.worker.js";
const pdfjsLib = require("pdfjs-dist");
pdfjsLib.GlobalWorkerOptions.workerPort = new PDFJSWorker();
import {
    Document,
    Packer,
    Paragraph,
    TextRun,
    ImageRun,
    FrameAnchorType,
    FrameWrap,
    HorizontalPositionRelativeFrom,
    VerticalPositionRelativeFrom
} from "docx";

export default function extractPdf(pdfData) {

    var extract = [];

    // Loading a document.
    pdfjsLib.getDocument({ data: pdfData }).promise
        .then(pdfDocument => {
            for (let i = 1; i < pdfDocument.numPages + 1; i++) {
                pdfDocument.getPage(i)
                    .then(pdfPage => {
                        // Getting text
                        extract.push({ children: getText(pdfPage) });
                        // Getting images
                        console.log(getImages(pdfPage));
                        // extract.push({ children: getImages(pdfPage) });
                    });
            }
        })
        .catch((reason) => {
            console.error("Error: " + reason);
        });

    function convertPDFtoTwip(coord) {
        return coord * 20
    };

    // retreive text in page
    function getText(pdfPage) {
        var res = [];
        pdfPage.getTextContent()
            .then(textContent => {
                var prev_x, prev_y, text;
                for (let j = 0; j < textContent.items.length; j++) {
                    var element = textContent.items[j];
                    var x = convertPDFtoTwip(element.transform[4]);
                    var y = convertPDFtoTwip(pdfPage.view[3] - element.transform[5]);
                    var width = convertPDFtoTwip(element.width);
                    var height = convertPDFtoTwip(element.height);
                    // console.log(x, y, prev_x, prev_y, width);
                    // if text boxes are adjacent, join them
                    // if ((Math.abs(x - prev_x + width) < 300) && (y - prev_y < 100) ) {
                    //   text += element.str;
                    //   console.log(text);
                    //   continue;
                    // } 
                    text = element.str;
                    prev_x = x;
                    prev_y = y;

                    // console.log(x, y, width, height);
                    res.push(new Paragraph({
                        frame: {
                            position: {
                                x: x,
                                y: y,
                            },
                            width: width,
                            height: height,
                            anchor: {
                                horizontal: FrameAnchorType.PAGE,
                                vertical: FrameAnchorType.PAGE,
                            },
                            wrap: {
                                type: FrameWrap.NONE,
                            },
                            alignment: {
                                x: HorizontalPositionRelativeFrom.PAGE,
                                y: VerticalPositionRelativeFrom.PAGE,
                            },
                        },
                        children: [
                            new TextRun({
                                text: text,
                                font: element.fontName,
                                size: Math.round(element.height * 2)
                            }),
                        ]
                    }));
                }
            });
        return res;
    };

    function getImages(pdfPage) {
        var res = [];
        var known = [pdfjsLib.OPS.paintJpegXObject, pdfjsLib.OPS.paintInlineImageXObject, pdfjsLib.OPS.paintXObject];
        console.log(pdfPage)
        // pdfPage.getOperatorList().then(function (ops) {
        //     for (var i = 0; i < ops.fnArray.length; i++) {
        //         if (ops.fnArray[i] === pdfjsLib.OPS.paintJpegXObject) {
        //             res.push(ops.argsArray[i][0])
        //         }
        //         if (ops.fnArray[i] === pdfjsLib.OPS.paintInlineImageXObject) {
        //             res.push(ops.argsArray[i][0])
        //         }
        //         if (ops.fnArray[i] === pdfjsLib.OPS.paintXObject) {
        //             res.push(ops.argsArray[i][0])
        //         }
        //     }
        // })
        res = pdfPage.objs._objs;
        return res;
    }

    return extract;
};