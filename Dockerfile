# syntax=docker/dockerfile:1

FROM node:12.18.1
ENV NODE_ENV=dev

WORKDIR /var/lib/www/annotator

CMD [ "npm", "run start" ]
